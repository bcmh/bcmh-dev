BCMH Development Plugin
=======================
Adds a blanket placeholder screen to the site on a publicly accessible server, preventing accidental views.

Credits
=======

Have used the brilliant Wordpress Sanity Plugin Framework as the plugin's foundation.
https://github.com/Emerson/Sanity-Wordpress-Plugin-Framework

To Do
=====

* Correctly incorporate Sanity framework
* Hook up option to toggle front end lock screen

